/*
 * Sample program for DocodeModem
 * transmit only
 * Copyright (c) 2020 Circuit Desgin,Inc
 * Released under the MIT license
 */
#include <docodemo.h>
#include <SlrModem.h>

#define SEND_PERIOD_MS 5000

const uint8_t CHANNEL = 0x10;  //10進で16チャネルです。通信相手と異なると通信できません。
const uint8_t DEVICE_DI = 0x00; //通信相手のIDです。0は全てに届きます。
const uint8_t DEVICE_EI = 0x1;  // 自分のIDです
const uint8_t DEVICE_GI = 0x02;//グループIDです。通信相手と異なると通信できません。

DOCODEMO Dm = DOCODEMO();
SlrModem modem;
HardwareSerial UartModem(MODEM_UART_NO);

void setup() {
  Dm.begin();//初期化が必要です。

  //デバッグ用シリアルの初期化です
  SerialDebug.begin(115200);
  while (!SerialDebug)
    ;

  //モデム用シリアルの初期化です。通信速度とポート番号を注意してください。
  UartModem.begin(MLR_BAUDRATE, SERIAL_8N1, MODEM_UART_RX_PORT, MODEM_UART_TX_PORT);
  while (!UartModem)
    ;

  //モデムの電源を入れて少し待ちます
  Dm.ModemPowerCtrl(ON);
  delay(150);

  //モデム操作用に初期化します
  modem.Init(UartModem, nullptr);

  //各無線設定を行います。電源入り切りするようであればtrueにして内蔵Flashに保存するようにしてください。
  modem.SetMode(SlrModemMode::LoRaCmd, false);
  modem.SetChannel(CHANNEL, false);
  modem.SetDestinationID(DEVICE_DI, false);
  modem.SetEquipmentID(DEVICE_EI, false);
  modem.SetGroupID(DEVICE_GI, false);
  
}

int count = 0;

void loop() {
  char data[10];

  while(1){
    //送信データ作成。下記の場合４バイトのASCIIですが、Binaryでも構いません。
    int size = sprintf(data, "%4d", count++);

    //@DT04****\r\nを実行し、送信結果を戻します。@DT04と\r\nは自動で付加されます。
    auto rc = modem.TransmitData((uint8_t *)data, size);
    if (rc == SlrModemError::Ok)
    {
      //送信完了
      SerialDebug.println("Send Ok");
    }
    else
    {
      //キャリアセンスによって送信できなかったことを示します
      SerialDebug.println("Send Ng...");
    }

    delay(3000); //LoRaだと通信速度が遅いので間隔をあける
  }
}